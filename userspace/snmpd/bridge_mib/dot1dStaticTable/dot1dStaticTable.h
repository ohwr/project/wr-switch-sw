/*
 * Note: this file originally auto-generated by mib2c using
 *       version $ of $
 *
 * $Id:$
 */
#ifndef DOT1DSTATICTABLE_H
#define DOT1DSTATICTABLE_H

#ifdef __cplusplus
extern "C" {
#endif


/** @addtogroup misc misc: Miscellaneous routines
 *
 * @{
 */
#include <net-snmp/library/asn1.h>

/* other required module components */
    /* *INDENT-OFF*  */
config_add_mib(BRIDGE-MIB)
config_require(BRIDGE-MIB/dot1dStaticTable/dot1dStaticTable_interface)
config_require(BRIDGE-MIB/dot1dStaticTable/dot1dStaticTable_data_access)
config_require(BRIDGE-MIB/dot1dStaticTable/dot1dStaticTable_data_get)
config_require(BRIDGE-MIB/dot1dStaticTable/dot1dStaticTable_data_set)
    /* *INDENT-ON*  */

/* OID and column number definitions for dot1dStaticTable */
#include "dot1dStaticTable_oids.h"

/* enum definions */
#include "dot1dStaticTable_enums.h"

/* *********************************************************************
 * function declarations
 */
void init_dot1dStaticTable(void);
void shutdown_dot1dStaticTable(void);

/* *********************************************************************
 * Table declarations
 */
/**********************************************************************
 **********************************************************************
 ***
 *** Table dot1dStaticTable
 ***
 **********************************************************************
 **********************************************************************/
/*
 * BRIDGE-MIB::dot1dStaticTable is subid 1 of dot1dStatic.
 * Its status is Current.
 * OID: .1.3.6.1.2.1.17.5.1, length: 9
*/
/* *********************************************************************
 * When you register your mib, you get to provide a generic
 * pointer that will be passed back to you for most of the
 * functions calls.
 *
 * TODO:100:r: Review all context structures
 */
    /*
     * TODO:101:o: |-> Review dot1dStaticTable registration context.
     */
typedef netsnmp_data_list dot1dStaticTable_registration;

/**********************************************************************/
/*
 * TODO:110:r: |-> Review dot1dStaticTable data context structure.
 * This structure is used to represent the data for dot1dStaticTable.
 */
/*
 * This structure contains storage for all the columns defined in the
 * dot1dStaticTable.
 */
typedef struct dot1dStaticTable_data_s {
    
        /*
         * dot1dStaticAllowedToGoTo(3)/OCTETSTR/ASN_OCTET_STR/char(char)//L/A/W/e/R/d/h
         */
   char   dot1dStaticAllowedToGoTo[512];
size_t      dot1dStaticAllowedToGoTo_len; /* # of char elements, not bytes */
    
        /*
         * dot1dStaticStatus(4)/INTEGER/ASN_INTEGER/long(u_long)//l/A/W/E/r/d/h
         */
   u_long   dot1dStaticStatus;
    
} dot1dStaticTable_data;


/*
 * TODO:120:r: |-> Review dot1dStaticTable mib index.
 * This structure is used to represent the index for dot1dStaticTable.
 */
typedef struct dot1dStaticTable_mib_index_s {

        /*
         * dot1dStaticAddress(1)/MacAddress/ASN_OCTET_STR/char(char)//L/A/W/e/R/d/H
         */
   char   dot1dStaticAddress[6];
   size_t      dot1dStaticAddress_len;

        /*
         * dot1dStaticReceivePort(2)/INTEGER32/ASN_INTEGER/long(long)//l/A/W/e/R/d/h
         */
   long   dot1dStaticReceivePort;


} dot1dStaticTable_mib_index;

    /*
     * TODO:121:r: |   |-> Review dot1dStaticTable max index length.
     * If you KNOW that your indexes will never exceed a certain
     * length, update this macro to that length.
     *
     * BE VERY CAREFUL TO TAKE INTO ACCOUNT THE MAXIMUM
     * POSSIBLE LENGHT FOR EVERY VARIABLE LENGTH INDEX!
     * Guessing 128 - col/entry(2)  - oid len(9)
*/
#define MAX_dot1dStaticTable_IDX_LEN     7


/* *********************************************************************
 * TODO:130:o: |-> Review dot1dStaticTable Row request (rowreq) context.
 * When your functions are called, you will be passed a
 * dot1dStaticTable_rowreq_ctx pointer.
 */
typedef struct dot1dStaticTable_rowreq_ctx_s {

    /** this must be first for container compare to work */
    netsnmp_index        oid_idx;
    oid                  oid_tmp[MAX_dot1dStaticTable_IDX_LEN];
    
    dot1dStaticTable_mib_index        tbl_idx;
    
    dot1dStaticTable_data              data;

    /*
     * flags per row. Currently, the first (lower) 8 bits are reserved
     * for the user. See mfd.h for other flags.
     */
    u_int                       rowreq_flags;

    /*
     * TODO:131:o: |   |-> Add useful data to dot1dStaticTable rowreq context.
     */
    
    /*
     * storage for future expansion
     */
    netsnmp_data_list             *dot1dStaticTable_data_list;

} dot1dStaticTable_rowreq_ctx;

typedef struct dot1dStaticTable_ref_rowreq_ctx_s {
    dot1dStaticTable_rowreq_ctx *rowreq_ctx;
} dot1dStaticTable_ref_rowreq_ctx;

/* *********************************************************************
 * function prototypes
 */
    int dot1dStaticTable_pre_request(dot1dStaticTable_registration * user_context);
    int dot1dStaticTable_post_request(dot1dStaticTable_registration * user_context,
        int rc);

    int dot1dStaticTable_rowreq_ctx_init(dot1dStaticTable_rowreq_ctx *rowreq_ctx,
                                   void *user_init_ctx);
    void dot1dStaticTable_rowreq_ctx_cleanup(dot1dStaticTable_rowreq_ctx *rowreq_ctx);


    dot1dStaticTable_rowreq_ctx *
                  dot1dStaticTable_row_find_by_mib_index(dot1dStaticTable_mib_index *mib_idx);

extern const oid dot1dStaticTable_oid[];
extern const int dot1dStaticTable_oid_size;


#include "dot1dStaticTable_interface.h"
#include "dot1dStaticTable_data_access.h"
#include "dot1dStaticTable_data_get.h"
#include "dot1dStaticTable_data_set.h"

/*
 * DUMMY markers, ignore
 *
 * TODO:099:x: *************************************************************
 * TODO:199:x: *************************************************************
 * TODO:299:x: *************************************************************
 * TODO:399:x: *************************************************************
 * TODO:499:x: *************************************************************
 */

#ifdef __cplusplus
}
#endif

#endif /* DOT1DSTATICTABLE_H */
/** @} */
