/*
 * Note: this file originally auto-generated by mib2c using
 *  $
 *
 * $Id:$
 */
#ifndef DOT1DSTATICTABLE_ENUMS_H
#define DOT1DSTATICTABLE_ENUMS_H

#ifdef __cplusplus
extern "C" {
#endif

 /*
 * NOTES on enums
 * ==============
 *
 * Value Mapping
 * -------------
 * If the values for your data type don't exactly match the
 * possible values defined by the mib, you should map them
 * below. For example, a boolean flag (1/0) is usually represented
 * as a TruthValue in a MIB, which maps to the values (1/2).
 *
 */
/*************************************************************************
 *************************************************************************
 *
 * enum definitions for table dot1dStaticTable
 *
 *************************************************************************
 *************************************************************************/

/*************************************************************
 * constants for enums for the MIB node
 * dot1dStaticStatus (INTEGER / ASN_INTEGER)
 *
 * since a Textual Convention may be referenced more than once in a
 * MIB, protect againt redefinitions of the enum values.
 */
#ifndef DOT1DSTATICSTATUS_ENUMS
#define DOT1DSTATICSTATUS_ENUMS

#define DOT1DSTATICSTATUS_OTHER  1 
#define DOT1DSTATICSTATUS_INVALID  2 
#define DOT1DSTATICSTATUS_PERMANENT  3 
#define DOT1DSTATICSTATUS_DELETEONRESET  4 
#define DOT1DSTATICSTATUS_DELETEONTIMEOUT  5 

#endif /* DOT1DSTATICSTATUS_ENUMS */

#ifdef __cplusplus
}
#endif

#endif /* DOT1DSTATICTABLE_ENUMS_H */
