/*
 * Note: this file originally auto-generated by mib2c using
 *       version $ of $
 *
 * $Id:$
 */
#ifndef DOT1QFDBTABLE_DATA_ACCESS_H
#define DOT1QFDBTABLE_DATA_ACCESS_H

#ifdef __cplusplus
extern "C" {
#endif


/* *********************************************************************
 * function declarations
 */

/* *********************************************************************
 * Table declarations
 */
/**********************************************************************
 **********************************************************************
 ***
 *** Table dot1qFdbTable
 ***
 **********************************************************************
 **********************************************************************/
/*
 * Q-BRIDGE-MIB::dot1qFdbTable is subid 1 of dot1qTp.
 * Its status is Current.
 * OID: .1.3.6.1.2.1.17.7.1.2.1, length: 11
*/


    int dot1qFdbTable_init_data(dot1qFdbTable_registration * dot1qFdbTable_reg);


    /*
     * TODO:180:o: Review dot1qFdbTable cache timeout.
     * The number of seconds before the cache times out
     */
#define DOT1QFDBTABLE_CACHE_TIMEOUT   60

void dot1qFdbTable_container_init(netsnmp_container **container_ptr_ptr,
                             netsnmp_cache *cache);
void dot1qFdbTable_container_shutdown(netsnmp_container *container_ptr);

int dot1qFdbTable_container_load(netsnmp_container *container);
void dot1qFdbTable_container_free(netsnmp_container *container);

int dot1qFdbTable_cache_load(netsnmp_container *container);
void dot1qFdbTable_cache_free(netsnmp_container *container);

    /*
    ***************************************************
    ***             START EXAMPLE CODE              ***
    ***---------------------------------------------***/
/* *********************************************************************
 * Since we have no idea how you really access your data, we'll go with
 * a worst case example: a flat text file.
 */
#define MAX_LINE_SIZE 256
    /*
    ***---------------------------------------------***
    ***              END  EXAMPLE CODE              ***
    ***************************************************/
    int dot1qFdbTable_row_prep( dot1qFdbTable_rowreq_ctx *rowreq_ctx);



#ifdef __cplusplus
}
#endif

#endif /* DOT1QFDBTABLE_DATA_ACCESS_H */
