/*
 * Note: this file originally auto-generated by mib2c using
 *  $
 *
 * $Id:$
 */
#ifndef DOT1QFDBTABLE_OIDS_H
#define DOT1QFDBTABLE_OIDS_H

#ifdef __cplusplus
extern "C" {
#endif


/* column number definitions for table dot1qFdbTable */
#define DOT1QFDBTABLE_OID              1,3,6,1,2,1,17,7,1,2,1


#define COLUMN_DOT1QFDBID         1
    
#define COLUMN_DOT1QFDBDYNAMICCOUNT         2
    

#define DOT1QFDBTABLE_MIN_COL   COLUMN_DOT1QFDBDYNAMICCOUNT
#define DOT1QFDBTABLE_MAX_COL   COLUMN_DOT1QFDBDYNAMICCOUNT
    


#ifdef __cplusplus
}
#endif

#endif /* DOT1QFDBTABLE_OIDS_H */
