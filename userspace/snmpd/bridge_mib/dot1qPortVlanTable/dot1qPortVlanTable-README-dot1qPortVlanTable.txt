************************************************************************
dot1qPortVlanTable README
------------------------------------------------------------------------
  This readme file describes the code generated by mib2c (using the MIBs
  for Dummies (MFD) configuration file). The code generated was
  generated specifically for the following SNMP table:

     dot1qPortVlanTable

  Your code will be called when the snmp agent receives requests for
  the dot1qPortVlanTable table.  The agent will start by looking for the right
  row in your existing data to operate on, if one exists.


  Configuration Variables
  ------------------------------------------------------------
  Some variables used for code generation may be set to affect the code
  generation. You may override these variables by setting them in the
  file defaults/table-dot1qPortVlanTable.m2d, and then re-running mib2c.

    m2c_table_settable (currently '0')
    --------------------------------------------------------
    This variable determines whether or not code is generated to support
    MIB object which have an access of read-write or read-create. The
    default is set based on whether or not the table contains writable
    objects, but can be over-ridden.

    Syntax: @eval $m2c_table_settable = 0@


    m2c_table_dependencies (currently '0')
    --------------------------------------------------------
    This variable determines whether or not code is generated to support
    checking dependencies between columns, rows or tables. The default
    is set based on whether or not the table contains writable objects,
    but can be over-ridden.

    Syntax: @eval $m2c_table_dependencies = 0@


    m2c_table_row_creation (currently '0')
    --------------------------------------------------------
    This variable determines whether or not code is generated to support
    checking creation of new rows via SNMP. The default is set based on
    whether or not the table contains read-create objects, but can be
    over-ridden.

    Syntax: @eval $m2c_table_row_creation = 0@


    m2c_context_reg (currently 'netsnmp_data_list')
    --------------------------------------------------------
    This variable contains the structure name to typedef for the
    dot1qPortVlanTable_registration.

    During initilization, you will provide a pointer to a structure of
    this type. This pointer is used as a parameter to many functions so
    that you have access to your registration data. The default is a
    netsnmp_data_list pointer, which will allow you to keep multiple
    pointers tagged by a text name. If you have a new or existing structure
    you would rather use, you can redefine this variable.
    

    To avoid regenerating code, you may also change this typedef directly
    in the dot1qPortVlanTable.h header.

    Syntax: @eval $m2c_context_reg = "struct my_registration_context@


    m2c_data_context (currently 'generated')
    --------------------------------------------------------
    This variable contains the structure name to typedef for the
    dot1qPortVlanTable_data.

    This typedef is used in the row request context structure for the table,
    dot1qPortVlanTable_rowreq_ctx.

    The typedef in the primary table context will be used for the data and
    undo structure types. This structure should contain all the data
    needed for all the columns in the table. The default is 'generated',
    which will cuase a new data strcuture to be generated with data members
    for each column.

    To avoid regenerating code, you may also change this typedef directly
    in the dot1qPortVlanTable.h header.

    Syntax: @eval $m2c_data_context = "struct my_data_context"@


    m2c_data_allocate (currently '0')
    --------------------------------------------------------
    This variable determines whether or not the data context (see above)
    requires memory to be allocated. The default generated data structure
    does not. If you are using a custom data context which needs to
    allocate memory, override this value and two additional functions
    will be generated:

      dot1qPortVlanTable_allocate_data
      dot1qPortVlanTable_release_data

    Syntax: @eval $m2c_data_allocate = 1@


    m2c_data_init (currently '1')
    --------------------------------------------------------
    This variable determines whether or not the data context (see above)
    or any other items you have added to the table context requires
    initialization. The default generated data structure does not. If you
    are using a custom data context or have added items needing initialization
    to the table context, override this value and two additional functions
    will be generated:

      dot1qPortVlanTable_rowreq_ctx_init
      dot1qPortVlanTable_rowreq_ctx_cleanup

    Syntax: @eval 1 = 1@


    m2c_table_access (currently 'container-cached')
    ------------------------------------------------------------------
    This variable determines which data interface will be use to generate
    code for looking up data for a given index. The default is the
    'container-cached' access code, which caches the data in a netsnmp-
    container (usually a sorted array).

    Available options can be determined by checking for mib2c configuration
    files that begin with 'mfd-access-*'.

    Syntax: @eval $m2c_table_access = 'container-cached'@

 
    m2c_include_examples (currently '1')
    ------------------------------------------------------------------
    This variable determines whether or not to generate example code. The
    default is to generate example code.

    Syntax: @eval $m2c_include_examples = 0@


    m2c_data_transient (currently '2')
    ------------------------------------------------------------------
    This variable determines how the generated example code deals with the
    data during data lookup. See the table readme file for details on how
    the current table access method interprets this value. In general,
    a value of 0 indicates persistent data, 1 indicates semi-transient and
    2 indicates transient data.

    Syntax: @eval $m2c_data_transient = 0@


 Index(es) for the dot1qPortVlanTable table
  ------------------------------------------------------------
  The index(es) for the dot1qPortVlanTable table are:

     dot1dBasePort:
        Syntax:      INTEGER32
        DataType:    INTEGER32
        ASN type:    ASN_INTEGER
        C-code type: long

  You should know how to set all these values from your data context,
  dot1qPortVlanTable_data.


************************************************************************
dot1qPortVlanTable File Overview
------------------------------------------------------------------------
  Several files have been generated to implement the dot1qPortVlanTable
  table. We'll go through these files, one by one, explaining each and
  letting you know which you need to edit.


File: dot1qPortVlanTable_data_access.[c|h]
------------------------------------------------------------------------
  The dot1qPortVlanTable_data_access file contains the interface to your data in
  its raw format.  These functions are used to build the row cache or 
  locate the row (depending on the table access method).

  Set MIB context
  -----------------
  TODO : Set MIB index values
  FUNC : dot1qPortVlanTable_indexes_set
  WHERE: dot1qPortVlanTable_data_access.c

  This is a convenience function for setting the index context from
  the native C data. Where necessary, value mapping should be done.

  This function should update the table index values (found in
  tbl_idx) for the given raw data.

  
  container summary
  ------------------------
    The container data access code is for cases when you want to
    store your data in the agent/sub-agent.

    ... to be continued...


  cache summary
  ------------------------
    The container-cached data access code is for cases when you want to
    cache your data in the agent/sub-agent.

    ... to be continued...




File: dot1qPortVlanTable_enums.h
------------------------------------------------------------------------
  This file contains macros for mapping enumeration values when the
  enumerated values defined by the MIB do not match the values used
  internally.

  Review this file to see if any values need to be updated.


File: dot1qPortVlanTable_data_get.c
------------------------------------------------------------------------
  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPvid_get

  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPortAcceptableFrameTypes_get

  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPortIngressFiltering_get

  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPortGvrpStatus_get

  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPortGvrpFailedRegistrations_get

  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPortGvrpLastPduOrigin_get

  Get data for column
  -------------------
  TODO : retrieve column data from raw data
  FUNC : dot1qPortRestrictedVlanRegistration_get



File: dot1qPortVlanTable_data_set.c
------------------------------------------------------------------------

  This table does not support set requests.


************************************************************************
dot1qPortVlanTable Reference
------------------------------------------------------------------------

Function flow
----------------------------------------------------
To give you the general idea of how the functions flow works, this
example flow is from a complete table implementation.

NOTE: Depending on your configuration, some of the functions used in the
      examples below  may not have been generated for the
      dot1qPortVlanTable table.

      Conversely, the examples below may not include some functions that
      were generated for the dot1qPortVlanTable table.

To watch the flow of the dot1qPortVlanTable table, use the
following debug tokens:

        snmp_agent
        helper:table:req
        dot1qPortVlanTable
        verbose:dot1qPortVlanTable
        internal:dot1qPortVlanTable

e.g.
        snmpd -f -Le -Ddot1qPortVlanTable,verbose:dot1qPortVlanTable,internal:dot1qPortVlanTable


Initialization
--------------------------------
init_xxxTable: called                           xxx.c
   initialize_table_xxxTable                    xxx.c
      _xxxTable_initialize_interface            xxx_interface.c
         xxxTable_init_data                     xxx_data_access.c
      _xxxTable_container_init                  xxx_interface.c
         xxxTable_container_init                xxx_data_access.c


GET Request
--------------------------------
_cache_load                                     xxx_interface.c
   xxxTable_cache_load                          xxx_data_access.c
      xxxTable_allocate_rowreq_ctx              xxx_interface.c
         xxxTable_allocate_data                 xxx_data_get.c
         xxxTable_rowreq_ctx_init               xxx_data_get.c
      xxxTable_indexes_set                      xxx_data_get.c
         xxxTable_indexes_set_tbl_idx           xxx_data_get.c

xxxTable_pre_request                              

_mfd_xxxTable_object_lookup                     xxx_interface.c
   xxxTable_row_prep                            xxx_data_access.c

_mfd_xxxTable_get_values                        xxx_interface.c
   _mfd_xxxTable_get_column                     xxx_interface.c
      yyy_get                                   xxx_data_get.c

xxxTable_post_request


GETNEXT Request
--------------------------------
_cache_load                                     ...
xxxTable_pre_request                            ...
_mfd_xxxTable_object_lookup                     ...
_mfd_xxxTable_get_values                        ...
xxxTable_post_request                           ...


SET Request: success
--------------------------------
_cache_load                                     ...
xxxTable_pre_request
_mfd_xxxTable_object_lookup                     ...

_mfd_xxxTable_check_objects                     xxx_interface.c
   _xxxTable_check_column                       xxx_interface.c
      yyy_check_value                           xxx_data_set.c

_mfd_xxxTable_undo_setup                        xxx_interface.c
   xxxTable_allocate_data                       ...
   xxxTable_undo_setup                          xxx_interface.c
      _xxxTable_undo_setup_column               xxx_interface.c
         yyy_undo_setup                         xxx_data_set.c

_mfd_xxxTable_set_values                        xxx_interface.c
   _xxxTable_set_column                         xxx_interface.c
      yyy_set                                   xxx_data_set.c

_mfd_xxxTable_check_dependencies                xxx_interface.c
   xxxTable_check_dependencies                  xxx_data_set.c

_mfd_xxxTable_commit                            xxx_interface.c
   xxxTable_commit                              xxx_data_set.c

_mfd_xxxTable_undo_cleanup                      xxx_interface.c
   xxxTable_undo_cleanup                        xxx_data_set.c
      xxxTable_release_data                     ...

xxxTable_post_request                           ...


SET Request: row creation
--------------------------------
_cache_load                                     ...
xxxTable_pre_request

_mfd_xxxTable_object_lookup                     ...
   xxxTable_index_from_oid                      xxx_interface.c
   xxxTable_allocate_rowreq_ctx                 ...
      ...
   _xxxTable_check_indexes                      xxx_interface.c
      yyy_check_index                           xxx_data_set.c
      xxxTable_validate_index                   xxx_data_set.c

_mfd_xxxTable_check_objects                     ...
   _xxxTable_check_column                       ...
      yyy_check_value                           ...
   _xxxTable_check_column                       ...
      yyy_check_value                           ...

_mfd_xxxTable_undo_setup                        ...
_mfd_xxxTable_set_values                        ...
_mfd_xxxTable_check_dependencies                ...
_mfd_xxxTable_commit                            ...
_mfd_xxxTable_undo_cleanup                      ...
xxxTable_post_request                           ...


SET Resuest: value error
--------------------------------
_cache_load                                     ...
xxxTable_pre_request                            ...
_mfd_xxxTable_object_lookup                     ...

_mfd_xxxTable_check_objects                     ...
   _xxxTable_check_column                       ...
      yyy_check_value                           ...
      ERROR:"yyy value not supported"

xxxTable_post_request                           ...


SET Request: commit failure
--------------------------------
_cache_load                                     ...
xxxTable_pre_request                            ...
_mfd_xxxTable_object_lookup                     ...
_mfd_xxxTable_check_objects                     ...
_mfd_xxxTable_undo_setup                        ...
_mfd_xxxTable_set_values                        ...
_mfd_xxxTable_check_dependencies                ...

_mfd_xxxTable_commit                            ...
   xxxTable_commit                              ...
   ERROR: bad rc -1

_mfd_xxxTable_undo_commit                       xxx_interface.c
   xxxTable_undo_commit                         xxx_data_set.c

_mfd_xxxTable_undo_values                       xxx_interface.c
   _xxxTable_undo_column                        xxx_interface.c
      yyy_undo                                  xxx_data_set.c

_mfd_xxxTable_undo_cleanup                      ...
xxxTable_post_request                           ...


Row release (user initiated)
--------------------------------
xxxTable_release_rowreq_ctx                     xxx_interface.c
   xxxTable_rowreq_ctx_cleanup                  xxx_data_get.c
   xxxTable_release_data                        xxx_data_get.c



Table / column details
----------------------------------------------------
/**********************************************************************
 **********************************************************************
 ***
 *** Table dot1qPortVlanTable
 ***
 **********************************************************************
 **********************************************************************/
/*
 * Q-BRIDGE-MIB::dot1qPortVlanTable is subid 5 of dot1qVlan.
 * Its status is Current.
 * OID: .1.3.6.1.2.1.17.7.1.4.5, length: 11
*/

/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPvid
 * dot1qPvid is subid 1 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadWrite.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.1
 * Description:
The PVID, the VLAN-ID assigned to untagged frames or
        Priority-Tagged frames received on this port.

        The value of this object MUST be retained across
        reinitializations of the management system.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  0      hasdefval 1
 *   readable   1     iscolumn 1     ranges 0      hashint   1
 *   settable   1
 *   defval: 1
 *   hint: d
 *
 *
 * Its syntax is VlanIndex (based on perltype UNSIGNED32)
 * The net-snmp type is ASN_UNSIGNED. The C type decl is u_long (u_long)
 */
/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPortAcceptableFrameTypes
 * dot1qPortAcceptableFrameTypes is subid 2 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadWrite.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.2
 * Description:
When this is admitOnlyVlanTagged(2), the device will
        discard untagged frames or Priority-Tagged frames
        received on this port.  When admitAll(1), untagged
        frames or Priority-Tagged frames received on this port
        will be accepted and assigned to a VID based on the
        PVID and VID Set for this port.

        This control does not affect VLAN-independent Bridge
        Protocol Data Unit (BPDU) frames, such as GVRP and
        Spanning Tree Protocol (STP).  It does affect VLAN-
        dependent BPDU frames, such as GMRP.

        The value of this object MUST be retained across
        reinitializations of the management system.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  1      hasdefval 1
 *   readable   1     iscolumn 1     ranges 0      hashint   0
 *   settable   1
 *   defval: admitAll
 *
 * Enum range: 2/8. Values:  admitAll(1), admitOnlyVlanTagged(2)
 *
 * Its syntax is INTEGER (based on perltype INTEGER)
 * The net-snmp type is ASN_INTEGER. The C type decl is long (u_long)
 */
/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPortIngressFiltering
 * dot1qPortIngressFiltering is subid 3 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadWrite.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.3
 * Description:
When this is true(1), the device will discard incoming
        frames for VLANs that do not include this Port in its

        Member set.  When false(2), the port will accept all
        incoming frames.

        This control does not affect VLAN-independent BPDU
        frames, such as GVRP and STP.  It does affect VLAN-
        dependent BPDU frames, such as GMRP.

        The value of this object MUST be retained across
        reinitializations of the management system.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  1      hasdefval 1
 *   readable   1     iscolumn 1     ranges 0      hashint   0
 *   settable   1
 *   defval: false
 *
 * Enum range: 2/8. Values:  true(1), false(2)
 *
 * Its syntax is TruthValue (based on perltype INTEGER)
 * The net-snmp type is ASN_INTEGER. The C type decl is long (u_long)
 */
/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPortGvrpStatus
 * dot1qPortGvrpStatus is subid 4 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadWrite.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.4
 * Description:
The state of GVRP operation on this port.  The value
        enabled(1) indicates that GVRP is enabled on this port,
        as long as dot1qGvrpStatus is also enabled for this
        device.  When disabled(2) but dot1qGvrpStatus is still
        enabled for the device, GVRP is disabled on this port:
        any GVRP packets received will be silently discarded, and
        no GVRP registrations will be propagated from other
        ports.  This object affects all GVRP Applicant and
        Registrar state machines on this port.  A transition
        from disabled(2) to enabled(1) will cause a reset of all
        GVRP state machines on this port.

        The value of this object MUST be retained across
        reinitializations of the management system.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  1      hasdefval 1
 *   readable   1     iscolumn 1     ranges 0      hashint   0
 *   settable   1
 *   defval: enabled
 *
 * Enum range: 1/8. Values:  enabled(1), disabled(2)
 *
 * Its syntax is EnabledStatus (based on perltype INTEGER)
 * The net-snmp type is ASN_INTEGER. The C type decl is long (u_long)
 */
/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPortGvrpFailedRegistrations
 * dot1qPortGvrpFailedRegistrations is subid 5 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadOnly.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.5
 * Description:
The total number of failed GVRP registrations, for any
        reason, on this port.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  0      hasdefval 0
 *   readable   1     iscolumn 1     ranges 0      hashint   0
 *   settable   0
 *
 *
 * Its syntax is COUNTER (based on perltype COUNTER)
 * The net-snmp type is ASN_COUNTER. The C type decl is u_long (u_long)
 */
/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPortGvrpLastPduOrigin
 * dot1qPortGvrpLastPduOrigin is subid 6 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadOnly.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.6
 * Description:
The Source MAC Address of the last GVRP message
        received on this port.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  0      hasdefval 0
 *   readable   1     iscolumn 1     ranges 1      hashint   1
 *   settable   0
 *   hint: 1x:
 *
 * Ranges:  6;
 *
 * Its syntax is MacAddress (based on perltype OCTETSTR)
 * The net-snmp type is ASN_OCTET_STR. The C type decl is char (char)
 * This data type requires a length.  (Max 6)
 */
/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qPortVlanEntry.dot1qPortRestrictedVlanRegistration
 * dot1qPortRestrictedVlanRegistration is subid 7 of dot1qPortVlanEntry.
 * Its status is Current, and its access level is ReadWrite.
 * OID: .1.3.6.1.2.1.17.7.1.4.5.1.7
 * Description:
The state of Restricted VLAN Registration on this port.
         If the value of this control is true(1), then creation
         of a new dynamic VLAN entry is permitted only if there
         is a Static VLAN Registration Entry for the VLAN concerned,
         in which the Registrar Administrative Control value for
         this port is Normal Registration.

        The value of this object MUST be retained across
        reinitializations of the management system.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  1      hasdefval 1
 *   readable   1     iscolumn 1     ranges 0      hashint   0
 *   settable   1
 *   defval: false
 *
 * Enum range: 2/8. Values:  true(1), false(2)
 *
 * Its syntax is TruthValue (based on perltype INTEGER)
 * The net-snmp type is ASN_INTEGER. The C type decl is long (u_long)
 */


