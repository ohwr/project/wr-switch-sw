/*
 * Note: this file originally auto-generated by mib2c using
 *       version $ of $
 *
 * $Id:$
 */
#ifndef DOT1QPORTVLANTABLE_DATA_ACCESS_H
#define DOT1QPORTVLANTABLE_DATA_ACCESS_H

#ifdef __cplusplus
extern "C" {
#endif


/* *********************************************************************
 * function declarations
 */

/* *********************************************************************
 * Table declarations
 */
/**********************************************************************
 **********************************************************************
 ***
 *** Table dot1qPortVlanTable
 ***
 **********************************************************************
 **********************************************************************/
/*
 * Q-BRIDGE-MIB::dot1qPortVlanTable is subid 5 of dot1qVlan.
 * Its status is Current.
 * OID: .1.3.6.1.2.1.17.7.1.4.5, length: 11
*/


    int dot1qPortVlanTable_init_data(dot1qPortVlanTable_registration * dot1qPortVlanTable_reg);


    /*
     * TODO:180:o: Review dot1qPortVlanTable cache timeout.
     * The number of seconds before the cache times out
     */
#define DOT1QPORTVLANTABLE_CACHE_TIMEOUT   60

void dot1qPortVlanTable_container_init(netsnmp_container **container_ptr_ptr,
                             netsnmp_cache *cache);
void dot1qPortVlanTable_container_shutdown(netsnmp_container *container_ptr);

int dot1qPortVlanTable_container_load(netsnmp_container *container);
void dot1qPortVlanTable_container_free(netsnmp_container *container);

int dot1qPortVlanTable_cache_load(netsnmp_container *container);
void dot1qPortVlanTable_cache_free(netsnmp_container *container);

    /*
    ***************************************************
    ***             START EXAMPLE CODE              ***
    ***---------------------------------------------***/
/* *********************************************************************
 * Since we have no idea how you really access your data, we'll go with
 * a worst case example: a flat text file.
 */
#define MAX_LINE_SIZE 256
    /*
    ***---------------------------------------------***
    ***              END  EXAMPLE CODE              ***
    ***************************************************/
    int dot1qPortVlanTable_row_prep( dot1qPortVlanTable_rowreq_ctx *rowreq_ctx);



#ifdef __cplusplus
}
#endif

#endif /* DOT1QPORTVLANTABLE_DATA_ACCESS_H */
