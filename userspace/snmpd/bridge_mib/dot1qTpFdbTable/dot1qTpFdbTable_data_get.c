/*
 * Note: this file originally auto-generated by mib2c using
 *       version $ of $ 
 *
 * $Id:$
 */
/* standard Net-SNMP includes */
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-features.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#include "wrsSnmp.h"
#include "snmp_shmem.h"

/* include our parent header */
#include "dot1qTpFdbTable.h"


/** @defgroup data_get data_get: Routines to get data
 *
 * TODO:230:M: Implement dot1qTpFdbTable get routines.
 * TODO:240:M: Implement dot1qTpFdbTable mapping routines (if any).
 *
 * These routine are used to get the value for individual objects. The
 * row context is passed, along with a pointer to the memory where the
 * value should be copied.
 *
 * @{
 */
/**********************************************************************
 **********************************************************************
 ***
 *** Table dot1qTpFdbTable
 ***
 **********************************************************************
 **********************************************************************/
/*
 * Q-BRIDGE-MIB::dot1qTpFdbTable is subid 2 of dot1qTp.
 * Its status is Current.
 * OID: .1.3.6.1.2.1.17.7.1.2.2, length: 11
*/

/* ---------------------------------------------------------------------
 * TODO:200:r: Implement dot1qTpFdbTable data context functions.
 */


/**
 * set mib index(es)
 *
 * @param tbl_idx mib index structure
 * @param dot1qFdbId_val
 * @param dot1qTpFdbAddress_ptr
 * @param dot1qTpFdbAddress_ptr_len
 *
 * @retval MFD_SUCCESS     : success.
 * @retval MFD_ERROR       : other error.
 *
 * @remark
 *  This convenience function is useful for setting all the MIB index
 *  components with a single function call. It is assume that the C values
 *  have already been mapped from their native/rawformat to the MIB format.
 */
int
dot1qTpFdbTable_indexes_set_tbl_idx(dot1qTpFdbTable_mib_index *tbl_idx, u_long dot1qFdbId_val, char *dot1qTpFdbAddress_val_ptr,  size_t dot1qTpFdbAddress_val_ptr_len)
{
    DEBUGMSGTL(("verbose:dot1qTpFdbTable:dot1qTpFdbTable_indexes_set_tbl_idx","called\n"));

    /* dot1qFdbId(1)/UNSIGNED32/ASN_UNSIGNED/u_long(u_long)//l/a/w/e/r/d/h */
    tbl_idx->dot1qFdbId = dot1qFdbId_val;
    
    /* dot1qTpFdbAddress(1)/MacAddress/ASN_OCTET_STR/char(char)//L/a/w/e/R/d/H */
    tbl_idx->dot1qTpFdbAddress_len = sizeof(tbl_idx->dot1qTpFdbAddress)/sizeof(tbl_idx->dot1qTpFdbAddress[0]); /* max length */
    /*
     * make sure there is enough space for dot1qTpFdbAddress data
     */
    if ((NULL == tbl_idx->dot1qTpFdbAddress) ||
        (tbl_idx->dot1qTpFdbAddress_len <
         (dot1qTpFdbAddress_val_ptr_len))) {
        snmp_log(LOG_ERR,"not enough space for value (dot1qTpFdbAddress_val_ptr)\n");
        return MFD_ERROR;
    }
    tbl_idx->dot1qTpFdbAddress_len = dot1qTpFdbAddress_val_ptr_len;
    memcpy( tbl_idx->dot1qTpFdbAddress, dot1qTpFdbAddress_val_ptr, dot1qTpFdbAddress_val_ptr_len* sizeof(dot1qTpFdbAddress_val_ptr[0]) );
    

    return MFD_SUCCESS;
} /* dot1qTpFdbTable_indexes_set_tbl_idx */

/**
 * @internal
 * set row context indexes
 *
 * @param reqreq_ctx the row context that needs updated indexes
 *
 * @retval MFD_SUCCESS     : success.
 * @retval MFD_ERROR       : other error.
 *
 * @remark
 *  This function sets the mib indexs, then updates the oid indexs
 *  from the mib index.
 */
int
dot1qTpFdbTable_indexes_set(dot1qTpFdbTable_rowreq_ctx *rowreq_ctx, u_long dot1qFdbId_val, char *dot1qTpFdbAddress_val_ptr,  size_t dot1qTpFdbAddress_val_ptr_len)
{
    DEBUGMSGTL(("verbose:dot1qTpFdbTable:dot1qTpFdbTable_indexes_set","called\n"));

    if(MFD_SUCCESS != dot1qTpFdbTable_indexes_set_tbl_idx(&rowreq_ctx->tbl_idx
                                   , dot1qFdbId_val
                                   , dot1qTpFdbAddress_val_ptr, dot1qTpFdbAddress_val_ptr_len
           ))
        return MFD_ERROR;

    /*
     * convert mib index to oid index
     */
    rowreq_ctx->oid_idx.len = sizeof(rowreq_ctx->oid_tmp) / sizeof(oid);
    if(0 != dot1qTpFdbTable_index_to_oid(&rowreq_ctx->oid_idx,
                                    &rowreq_ctx->tbl_idx)) {
        return MFD_ERROR;
    }

    return MFD_SUCCESS;
} /* dot1qTpFdbTable_indexes_set */


/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qTpFdbEntry.dot1qTpFdbPort
 * dot1qTpFdbPort is subid 2 of dot1qTpFdbEntry.
 * Its status is Current, and its access level is ReadOnly.
 * OID: .1.3.6.1.2.1.17.7.1.2.2.1.2
 * Description:
Either the value '0', or the port number of the port on
        which a frame having a source address equal to the value
        of the corresponding instance of dot1qTpFdbAddress has
        been seen.  A value of '0' indicates that the port
        number has not been learned but that the device does
        have some forwarding/filtering information about this
        address (e.g., in the dot1qStaticUnicastTable).
        Implementors are encouraged to assign the port value to
        this object whenever it is learned, even for addresses
        for which the corresponding value of dot1qTpFdbStatus is
        not learned(3).
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  0      hasdefval 0
 *   readable   1     iscolumn 1     ranges 1      hashint   0
 *   settable   0
 *
 * Ranges:  0 - 65535;
 *
 * Its syntax is INTEGER32 (based on perltype INTEGER32)
 * The net-snmp type is ASN_INTEGER. The C type decl is long (long)
 */
/**
 * Extract the current value of the dot1qTpFdbPort data.
 *
 * Set a value using the data context for the row.
 *
 * @param rowreq_ctx
 *        Pointer to the row request context.
 * @param dot1qTpFdbPort_val_ptr
 *        Pointer to storage for a long variable
 *
 * @retval MFD_SUCCESS         : success
 * @retval MFD_SKIP            : skip this node (no value for now)
 * @retval MFD_ERROR           : Any other error
 */
int
dot1qTpFdbPort_get( dot1qTpFdbTable_rowreq_ctx *rowreq_ctx, long * dot1qTpFdbPort_val_ptr )
{
   /** we should have a non-NULL pointer */
   netsnmp_assert( NULL != dot1qTpFdbPort_val_ptr );


    DEBUGMSGTL(("verbose:dot1qTpFdbTable:dot1qTpFdbPort_get","called\n"));

    netsnmp_assert(NULL != rowreq_ctx);

/*
 * TODO:231:o: |-> Extract the current value of the dot1qTpFdbPort data.
 * copy (* dot1qTpFdbPort_val_ptr ) from rowreq_ctx->data
 */
    (* dot1qTpFdbPort_val_ptr ) = rowreq_ctx->data.dot1qTpFdbPort;

    return MFD_SUCCESS;
} /* dot1qTpFdbPort_get */

/*---------------------------------------------------------------------
 * Q-BRIDGE-MIB::dot1qTpFdbEntry.dot1qTpFdbStatus
 * dot1qTpFdbStatus is subid 3 of dot1qTpFdbEntry.
 * Its status is Current, and its access level is ReadOnly.
 * OID: .1.3.6.1.2.1.17.7.1.2.2.1.3
 * Description:
The status of this entry.  The meanings of the values
        are:
            other(1) - none of the following.  This may include
                the case where some other MIB object (not the
                corresponding instance of dot1qTpFdbPort, nor an
                entry in the dot1qStaticUnicastTable) is being
                used to determine if and how frames addressed to
                the value of the corresponding instance of
                dot1qTpFdbAddress are being forwarded.
            invalid(2) - this entry is no longer valid (e.g., it

                was learned but has since aged out), but has not
                yet been flushed from the table.
            learned(3) - the value of the corresponding instance
                of dot1qTpFdbPort was learned and is being used.
            self(4) - the value of the corresponding instance of
                dot1qTpFdbAddress represents one of the device's
                addresses.  The corresponding instance of
                dot1qTpFdbPort indicates which of the device's
                ports has this address.
            mgmt(5) - the value of the corresponding instance of
                dot1qTpFdbAddress is also the value of an
                existing instance of dot1qStaticAddress.
 *
 * Attributes:
 *   accessible 1     isscalar 0     enums  1      hasdefval 0
 *   readable   1     iscolumn 1     ranges 0      hashint   0
 *   settable   0
 *
 * Enum range: 4/8. Values:  other(1), invalid(2), learned(3), self(4), mgmt(5)
 *
 * Its syntax is INTEGER (based on perltype INTEGER)
 * The net-snmp type is ASN_INTEGER. The C type decl is long (u_long)
 */
/**
 * map a value from its original native format to the MIB format.
 *
 * @retval MFD_SUCCESS         : success
 * @retval MFD_ERROR           : Any other error
 *
 * @note parameters follow the memset convention (dest, src).
 *
 * @note generation and use of this function can be turned off by re-running
 * mib2c after adding the following line to the file
 * defaults/node-dot1qTpFdbStatus.m2d :
 *   @eval $m2c_node_skip_mapping = 1@
 *
 * @remark
 *  If the values for your data type don't exactly match the
 *  possible values defined by the mib, you should map them here.
 *  Otherwise, just do a direct copy.
 */
int
dot1qTpFdbStatus_map(u_long *mib_dot1qTpFdbStatus_val_ptr, u_long raw_dot1qTpFdbStatus_val)
{
    netsnmp_assert(NULL != mib_dot1qTpFdbStatus_val_ptr);

    DEBUGMSGTL(("verbose:dot1qTpFdbTable:dot1qTpFdbStatus_map","called\n"));

    /*
     * TODO:241:o: |-> Implement dot1qTpFdbStatus enum mapping.
     * uses INTERNAL_* macros defined in the header files
     */
    switch(raw_dot1qTpFdbStatus_val) {
        case RTU_ENTRY_TYPE_DYNAMIC:
             *mib_dot1qTpFdbStatus_val_ptr = DOT1QTPFDBSTATUS_LEARNED;
             break;

        case RTU_ENTRY_TYPE_STATIC:
             *mib_dot1qTpFdbStatus_val_ptr = DOT1QTPFDBSTATUS_MGMT;
             break;

        default:
             *mib_dot1qTpFdbStatus_val_ptr = DOT1QTPFDBSTATUS_OTHER;
             break;
    }

    return MFD_SUCCESS;
} /* dot1qTpFdbStatus_map */

/**
 * Extract the current value of the dot1qTpFdbStatus data.
 *
 * Set a value using the data context for the row.
 *
 * @param rowreq_ctx
 *        Pointer to the row request context.
 * @param dot1qTpFdbStatus_val_ptr
 *        Pointer to storage for a long variable
 *
 * @retval MFD_SUCCESS         : success
 * @retval MFD_SKIP            : skip this node (no value for now)
 * @retval MFD_ERROR           : Any other error
 */
int
dot1qTpFdbStatus_get( dot1qTpFdbTable_rowreq_ctx *rowreq_ctx, u_long * dot1qTpFdbStatus_val_ptr )
{
   /** we should have a non-NULL pointer */
   netsnmp_assert( NULL != dot1qTpFdbStatus_val_ptr );


    DEBUGMSGTL(("verbose:dot1qTpFdbTable:dot1qTpFdbStatus_get","called\n"));

    netsnmp_assert(NULL != rowreq_ctx);

/*
 * TODO:231:o: |-> Extract the current value of the dot1qTpFdbStatus data.
 * copy (* dot1qTpFdbStatus_val_ptr ) from rowreq_ctx->data
 */
    (* dot1qTpFdbStatus_val_ptr ) = rowreq_ctx->data.dot1qTpFdbStatus;

    return MFD_SUCCESS;
} /* dot1qTpFdbStatus_get */



/** @} */
