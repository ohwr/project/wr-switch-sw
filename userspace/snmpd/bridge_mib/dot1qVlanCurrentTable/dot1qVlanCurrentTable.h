/*
 * Note: this file originally auto-generated by mib2c using
 *       version $ of $
 *
 * $Id:$
 */
#ifndef DOT1QVLANCURRENTTABLE_H
#define DOT1QVLANCURRENTTABLE_H

#ifdef __cplusplus
extern "C" {
#endif


/** @addtogroup misc misc: Miscellaneous routines
 *
 * @{
 */
#include <net-snmp/library/asn1.h>

/* other required module components */
    /* *INDENT-OFF*  */
config_add_mib(Q-BRIDGE-MIB)
config_require(Q-BRIDGE-MIB/dot1qVlanCurrentTable/dot1qVlanCurrentTable_interface)
config_require(Q-BRIDGE-MIB/dot1qVlanCurrentTable/dot1qVlanCurrentTable_data_access)
config_require(Q-BRIDGE-MIB/dot1qVlanCurrentTable/dot1qVlanCurrentTable_data_get)
config_require(Q-BRIDGE-MIB/dot1qVlanCurrentTable/dot1qVlanCurrentTable_data_set)
    /* *INDENT-ON*  */

/* OID and column number definitions for dot1qVlanCurrentTable */
#include "dot1qVlanCurrentTable_oids.h"

/* enum definions */
#include "dot1qVlanCurrentTable_enums.h"

/* *********************************************************************
 * function declarations
 */
void init_dot1qVlanCurrentTable(void);
void shutdown_dot1qVlanCurrentTable(void);

/* *********************************************************************
 * Table declarations
 */
/**********************************************************************
 **********************************************************************
 ***
 *** Table dot1qVlanCurrentTable
 ***
 **********************************************************************
 **********************************************************************/
/*
 * Q-BRIDGE-MIB::dot1qVlanCurrentTable is subid 2 of dot1qVlan.
 * Its status is Current.
 * OID: .1.3.6.1.2.1.17.7.1.4.2, length: 11
*/
/* *********************************************************************
 * When you register your mib, you get to provide a generic
 * pointer that will be passed back to you for most of the
 * functions calls.
 *
 * TODO:100:r: Review all context structures
 */
    /*
     * TODO:101:o: |-> Review dot1qVlanCurrentTable registration context.
     */
typedef netsnmp_data_list dot1qVlanCurrentTable_registration;

/**********************************************************************/
/*
 * TODO:110:r: |-> Review dot1qVlanCurrentTable data context structure.
 * This structure is used to represent the data for dot1qVlanCurrentTable.
 */
/*
 * This structure contains storage for all the columns defined in the
 * dot1qVlanCurrentTable.
 */
typedef struct dot1qVlanCurrentTable_data_s {
    
        /*
         * dot1qVlanFdbId(3)/UNSIGNED32/ASN_UNSIGNED/u_long(u_long)//l/A/w/e/r/d/h
         */
   u_long   dot1qVlanFdbId;
    
        /*
         * dot1qVlanCurrentEgressPorts(4)/PortList/ASN_OCTET_STR/char(char)//L/A/w/e/r/d/h
         */
   char   dot1qVlanCurrentEgressPorts[16];
size_t      dot1qVlanCurrentEgressPorts_len; /* # of char elements, not bytes */
    
        /*
         * dot1qVlanCurrentUntaggedPorts(5)/PortList/ASN_OCTET_STR/char(char)//L/A/w/e/r/d/h
         */
   char   dot1qVlanCurrentUntaggedPorts[16];
size_t      dot1qVlanCurrentUntaggedPorts_len; /* # of char elements, not bytes */
    
        /*
         * dot1qVlanStatus(6)/INTEGER/ASN_INTEGER/long(u_long)//l/A/w/E/r/d/h
         */
   u_long   dot1qVlanStatus;
    
        /*
         * dot1qVlanCreationTime(7)/TICKS/ASN_TIMETICKS/u_long(u_long)//l/A/w/e/r/d/h
         */
   u_long   dot1qVlanCreationTime;
    
} dot1qVlanCurrentTable_data;


/*
 * TODO:120:r: |-> Review dot1qVlanCurrentTable mib index.
 * This structure is used to represent the index for dot1qVlanCurrentTable.
 */
typedef struct dot1qVlanCurrentTable_mib_index_s {

        /*
         * dot1qVlanTimeMark(1)/TimeFilter/ASN_TIMETICKS/u_long(u_long)//l/a/w/e/r/d/h
         */
   u_long   dot1qVlanTimeMark;

        /*
         * dot1qVlanIndex(2)/VlanIndex/ASN_UNSIGNED/u_long(u_long)//l/a/w/e/r/d/H
         */
   u_long   dot1qVlanIndex;


} dot1qVlanCurrentTable_mib_index;

    /*
     * TODO:121:r: |   |-> Review dot1qVlanCurrentTable max index length.
     * If you KNOW that your indexes will never exceed a certain
     * length, update this macro to that length.
*/
#define MAX_dot1qVlanCurrentTable_IDX_LEN     2


/* *********************************************************************
 * TODO:130:o: |-> Review dot1qVlanCurrentTable Row request (rowreq) context.
 * When your functions are called, you will be passed a
 * dot1qVlanCurrentTable_rowreq_ctx pointer.
 */
typedef struct dot1qVlanCurrentTable_rowreq_ctx_s {

    /** this must be first for container compare to work */
    netsnmp_index        oid_idx;
    oid                  oid_tmp[MAX_dot1qVlanCurrentTable_IDX_LEN];
    
    dot1qVlanCurrentTable_mib_index        tbl_idx;
    
    dot1qVlanCurrentTable_data              data;

    /*
     * flags per row. Currently, the first (lower) 8 bits are reserved
     * for the user. See mfd.h for other flags.
     */
    u_int                       rowreq_flags;

    /*
     * TODO:131:o: |   |-> Add useful data to dot1qVlanCurrentTable rowreq context.
     */
    
    /*
     * storage for future expansion
     */
    netsnmp_data_list             *dot1qVlanCurrentTable_data_list;

} dot1qVlanCurrentTable_rowreq_ctx;

typedef struct dot1qVlanCurrentTable_ref_rowreq_ctx_s {
    dot1qVlanCurrentTable_rowreq_ctx *rowreq_ctx;
} dot1qVlanCurrentTable_ref_rowreq_ctx;

/* *********************************************************************
 * function prototypes
 */
    int dot1qVlanCurrentTable_pre_request(dot1qVlanCurrentTable_registration * user_context);
    int dot1qVlanCurrentTable_post_request(dot1qVlanCurrentTable_registration * user_context,
        int rc);

    int dot1qVlanCurrentTable_rowreq_ctx_init(dot1qVlanCurrentTable_rowreq_ctx *rowreq_ctx,
                                   void *user_init_ctx);
    void dot1qVlanCurrentTable_rowreq_ctx_cleanup(dot1qVlanCurrentTable_rowreq_ctx *rowreq_ctx);


    dot1qVlanCurrentTable_rowreq_ctx *
                  dot1qVlanCurrentTable_row_find_by_mib_index(dot1qVlanCurrentTable_mib_index *mib_idx);

extern const oid dot1qVlanCurrentTable_oid[];
extern const int dot1qVlanCurrentTable_oid_size;


#include "dot1qVlanCurrentTable_interface.h"
#include "dot1qVlanCurrentTable_data_access.h"
#include "dot1qVlanCurrentTable_data_get.h"
#include "dot1qVlanCurrentTable_data_set.h"

/*
 * DUMMY markers, ignore
 *
 * TODO:099:x: *************************************************************
 * TODO:199:x: *************************************************************
 * TODO:299:x: *************************************************************
 * TODO:399:x: *************************************************************
 * TODO:499:x: *************************************************************
 */

#ifdef __cplusplus
}
#endif

#endif /* DOT1QVLANCURRENTTABLE_H */
/** @} */
