/*
 * Note: this file originally auto-generated by mib2c using
 *       version $ of $
 *
 * $Id:$
 */
/** @ingroup interface: Routines to interface to Net-SNMP
 *
 * \warning This code should not be modified, called directly,
 *          or used to interpret functionality. It is subject to
 *          change at any time.
 * 
 * @{
 */
/*
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 * ***                                                               ***
 * ***  NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE  ***
 * ***                                                               ***
 * ***                                                               ***
 * ***       THIS FILE DOES NOT CONTAIN ANY USER EDITABLE CODE.      ***
 * ***                                                               ***
 * ***                                                               ***
 * ***       THE GENERATED CODE IS INTERNAL IMPLEMENTATION, AND      ***
 * ***                                                               ***
 * ***                                                               ***
 * ***    IS SUBJECT TO CHANGE WITHOUT WARNING IN FUTURE RELEASES.   ***
 * ***                                                               ***
 * ***                                                               ***
 * *********************************************************************
 * *********************************************************************
 * *********************************************************************
 */
#ifndef DOT1QVLANSTATICTABLE_INTERFACE_H
#define DOT1QVLANSTATICTABLE_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif


#include "dot1qVlanStaticTable.h"


/* ********************************************************************
 * Table declarations
 */

/* PUBLIC interface initialization routine */
void _dot1qVlanStaticTable_initialize_interface(dot1qVlanStaticTable_registration * user_ctx,
                                    u_long flags);
void _dot1qVlanStaticTable_shutdown_interface(dot1qVlanStaticTable_registration * user_ctx);

dot1qVlanStaticTable_registration *
dot1qVlanStaticTable_registration_get( void );

dot1qVlanStaticTable_registration *
dot1qVlanStaticTable_registration_set( dot1qVlanStaticTable_registration * newreg );

netsnmp_container *dot1qVlanStaticTable_container_get( void );
int dot1qVlanStaticTable_container_size( void );

    dot1qVlanStaticTable_rowreq_ctx * dot1qVlanStaticTable_allocate_rowreq_ctx(void *);
void dot1qVlanStaticTable_release_rowreq_ctx(dot1qVlanStaticTable_rowreq_ctx *rowreq_ctx);

int dot1qVlanStaticTable_index_to_oid(netsnmp_index *oid_idx,
                            dot1qVlanStaticTable_mib_index *mib_idx);
int dot1qVlanStaticTable_index_from_oid(netsnmp_index *oid_idx,
                              dot1qVlanStaticTable_mib_index *mib_idx);

/*
 * access to certain internals. use with caution!
 */
void dot1qVlanStaticTable_valid_columns_set(netsnmp_column_info *vc);


#ifdef __cplusplus
}
#endif

#endif /* DOT1QVLANSTATICTABLE_INTERFACE_H */
/** @} */
